const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const postcssPresetEnv = require('postcss-preset-env');

const htmlPlugin = new HtmlWebPackPlugin({
    template: './src/index.html',
});

module.exports = {
    entry: './src/createSlider.js',
    output: {
        path: path.resolve('dist'),
        publicPath: '/',
        filename: 'main.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            ['@babel/plugin-transform-runtime', {
                                regenerator: true,
                            }],
                        ],
                    },
                }],
            }, {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            ['@babel/plugin-transform-runtime', {
                                regenerator: true,
                            }],
                        ],
                    },
                }],
            }, {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                        // you can specify a publicPath here
                        // by default it use publicPath in webpackOptions.output
                            publicPath: '/',
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                postcssPresetEnv({
                                    features: {
                                        'nesting-rules': true,
                                        'custom-media-queries': true,
                                    },
                                    stage: 2,
                                }),
                            ],
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        htmlPlugin,
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are  optional
            filename: 'styles.css',
        }),
    ],
    resolve: {
        extensions: ['.js'],
    },
};
