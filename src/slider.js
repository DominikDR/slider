import './styles.css';

const imagesPaths = [
    "./images/forest.jpg",
    "./images/road.jpg",
    "./images/road2.jpg",
    "./images/road3.jpg",
    "./images/spring.jpg",
];

const imagesPaths2 = [
    "./images/spring.jpg",
    "./images/road.jpg",
    "./images/road2.jpg",
    "./images/road3.jpg",
    "./images/forest.jpg",
];

const createSlider = (data, id) => {
    let currentSlide = 0;
    const sliderContainer = document.getElementById(id);

    const goToSlide = (n) => {
        const slider = sliderContainer.querySelectorAll('.slider .slide');
        const dot = sliderContainer.querySelectorAll('.dot-box .dot');
        dot[currentSlide].className = 'dot';
        slider[currentSlide].className = 'slide';
        currentSlide = (n + slider.length) % slider.length;
        slider[currentSlide].className = 'slide showing';
        dot[currentSlide].className = 'dot current';
        resetInterval();
    };

    const createImageList = (images) => (`
        <div>
            ${images.map((image, index) => `<img class="${index === 0 ? 'slide showing' : 'slide'}" src=${image} >`).join('')}
        </div>
    `);

    const createControlArrows = () => (`
        <div class="control-buttons">
            <button class="controls controls-prev">&lt;</button>
            <button class="controls controls-next">&gt;</button>
        </div>
    `);

    const createButtonDotsList = (arrayElements) => {
        const dotBox = document.createElement('div');
        dotBox.className = 'dot-box';

        arrayElements.forEach((el,index) => {
            const dotButton = document.createElement('div');

            dotButton.className = index === 0 ? 'dot current' : 'dot';
            dotButton.addEventListener('click', goToSlide.bind(null, index))

            dotBox.appendChild(dotButton);
        });

        return dotBox;
    };

    const goToNextSlide = () => {
        goToSlide(currentSlide + 1);
    };

    const goToPreviousSlide = () => {
        goToSlide(currentSlide - 1);
    };

    let slideInterval = setInterval(goToNextSlide, 2000);

    const resetInterval = () => {
        clearInterval(slideInterval);
        slideInterval = setInterval(goToNextSlide, 2000);
    };

    const addControlAbility = () => {
        const next = sliderContainer.querySelector('.controls-next');
        const previous = sliderContainer.querySelector('.controls-prev');

        next.addEventListener('click', goToNextSlide);
        previous.addEventListener('click', goToPreviousSlide);
    };

    const sliderDiv = `
        <div class="slider">
            ${createImageList(data)}
            ${createControlArrows()}
        </div>
    `;

    sliderContainer.insertAdjacentHTML('beforeend', sliderDiv);
    sliderContainer.querySelector('.slider').appendChild(createButtonDotsList(data));

    addControlAbility();
};

createSlider(imagesPaths, 'slider-1');
createSlider(imagesPaths2, 'slider-2');
