import { data } from './data';
import { animationClasses } from './textAnimationClasses';
import './styles.css';

const createSlider = (data, id, rootClassName) => {
    let currentSlide = 0;
    let slideInterval;
    const sliderRoot = document.getElementById(id);
    sliderRoot.className = rootClassName;

    const getRandomAnimation = (animations) => {
        const random = Math.floor(Math.random() * animations.length);
        return animations[random];
    };

    const goToSlide = (n) => {
        const slider = sliderRoot.querySelectorAll('.slider-container .slider-image');
        const dot = sliderRoot.querySelectorAll('.slider-button-dot-box .slider-button-dot');
        const text = sliderRoot.querySelectorAll('.slider-content-box .slider-text');
        dot[currentSlide].className = 'slider-button-dot';
        slider[currentSlide].className = 'slider-image';
        text[currentSlide].className = 'slider-text';
        currentSlide = (n + slider.length) % slider.length;
        slider[currentSlide].className = 'slider-image slider-image-showing';
        dot[currentSlide].className = 'slider-button-dot slider-button-dot-current';
        text[currentSlide].className = `slider-text ${getRandomAnimation(animationClasses)}`;
        playSlideshow();
    };

    const createSlideList = (data) => (`
        <div class="slider-content-box">
            ${data.map((el, index) => `
                <img class="${index === 0 ? 'slider-image slider-image-showing' : 'slider-image'}" src=${el.image}>
                <h3 class="${index === 0 ? `slider-text ${getRandomAnimation(animationClasses)}` : 'slider-text'}">${el.text}</h3>
            `).join('')}
        </div>
    `);

    const createControlArrows = () => {
        const buttonBox = document.createElement('div');
        const nextSLideButton = document.createElement('button');
        const previousSlideButton = document.createElement('button');

        buttonBox.className = 'slider-control-buttons';
        previousSlideButton.className = 'slider-controls slider-controls-prev';
        nextSLideButton.className = 'slider-controls slider-controls-next';

        previousSlideButton.innerHTML = '&lt;';
        nextSLideButton.innerHTML = '&gt;';
        previousSlideButton.addEventListener('click', goToPreviousSlide);
        nextSLideButton.addEventListener('click', goToNextSlide);

        previousSlideButton.onkeydown = addKeyboardArrowsControl;
        nextSLideButton.onkeydown = addKeyboardArrowsControl;

        buttonBox.appendChild(previousSlideButton);
        buttonBox.appendChild(nextSLideButton);

        return buttonBox;
    };

    const createButtonDotsList = (arrayElements) => {
        const dotBox = document.createElement('div');
        dotBox.className = 'slider-button-dot-box';

        arrayElements.forEach((el,index) => {
            const dotButton = document.createElement('div');

            dotButton.className = index === 0 ? 'slider-button-dot slider-button-dot-current' : 'slider-button-dot';
            dotButton.addEventListener('click', goToSlide.bind(null, index))

            dotBox.appendChild(dotButton);
        });

        return dotBox;
    };

    const goToNextSlide = () => {
        goToSlide(currentSlide + 1);
    };

    const goToPreviousSlide = () => {
        goToSlide(currentSlide - 1);
    };

    const setSlideInterval = () => {
        return setInterval(goToNextSlide, 3000);
    };
    slideInterval = setSlideInterval();

    const pauseSlideshow = () => {
        clearInterval(slideInterval);
    };

    const playSlideshow = () => {
        clearInterval(slideInterval);
        slideInterval = setSlideInterval();
    };

    const stopOnHover = () => {
        const imageBox = sliderRoot.querySelector('.slider-content-box');
        imageBox.addEventListener('mouseover', pauseSlideshow);
        imageBox.addEventListener('mouseout', playSlideshow);
    };

    const addKeyboardArrowsControl = (e) => {
        const leftKeyboardArrow = 37;
        const rightKeyboardArrow = 39;

        if (e.keyCode === leftKeyboardArrow) {
            goToPreviousSlide();
        };
        if (e.keyCode === rightKeyboardArrow) {
            goToNextSlide();
        };
    };

    const sliderDiv = document.createElement('div');
    sliderDiv.className = 'slider-container';

    sliderDiv.insertAdjacentHTML('beforeend', createSlideList(data));
    sliderDiv.appendChild(createControlArrows());
    sliderDiv.appendChild(createButtonDotsList(data))

    sliderRoot.appendChild(sliderDiv);

    stopOnHover();
};

createSlider(data, 'slider-1', 'slider-root');
