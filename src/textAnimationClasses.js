const animationClasses = [
    'roll-in-left',
    'focus-in-expand-fwd',
    'focus-in-contract', 
    'focus-in-contract-bck',
    'tracking-in-expand',
    'tracking-in-expand-fwd-top',
    'tracking-in-contract-bck',
    'tracking-in-contract-bck-bottom',
];

export { animationClasses };
