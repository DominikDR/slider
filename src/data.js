const data = [
    {
        id: 1,
        image: "./images/forest.jpg",
        text: "Lorem ipsum is"

    },
    {
        id: 2,
        image: "./images/road.jpg",
        text: "a pseudo-Latin text",
    },
    {
        id: 3,
        image: "./images/road2.jpg",
        text: "used in web design",
    },
    {
        id: 4,
        image: "./images/road3.jpg",
        text: "typography, layout, and printing",
    },
    {
        id: 5,
        image: "./images/spring.jpg",
        text: "to emphasise design elements over content.",
    }
];

export { data };
